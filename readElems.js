// these functions will read the children of 'source_elem'
// and set relevant events
// based on class names
// to their 'js-funct' attribute
//
// great for unobtrusive js
// and dynamic page changes.

// ive commented out things im not currently using


function readElems(source_elem) {
  elems = $( source_elem ).find('.js');
  console.log('readElems ' + elems.length);
  for (i=0;i<elems.length;i++) {
    setFunction(elems[i]);
  }
}

function setFunction(elem) {
  console.log('setFunction');
  if ($(elem).attr('click')) {
    elem.onclick = functionalize($(elem).attr('click'));
  }
  if ($(elem).attr('change')) {
    elem.onchange = functionalize($(elem).attr('change'));
  }
}

function functionalize(v) {
  return window[v];
}

// function readElems(source_elem) {
//   console.log('reading');
//   setOnClick(source_elem);
//   setOnChange(source_elem);
//   setOnLoad(source_elem);
//   setOnUnload(source_elem);
//   setOnSubmit(source_elem);
// }
// 
// function setOnClick(source_elem) {
//   console.log('set onclick');
//   var clicks = $( source_elem ).find(".js-onclick");
//   for (i=0; i<clicks.length; i++) {
//     console.log(clicks[i].text);
//     clicks[i].onclick = setFunction;
//   }
// }
// 
// function setFunction(funct_type) {
//   console.log('setFunction');
//   window[this.getAttribute('js_funct')](this);
// }

// function setOnChange(source_elem) {
//   var changes = $( source_elem ).find(".js-onchange");
//   for (i=0; i<changes.length; i++) {
//     changes[i].onchange = setSelectFunction;
//   }
// }
// 
// function setOnLoad(source_elem) {
//   console.log('set onload');
//   var onloads = $( source_elem ).find(".js-onload");
//   for (i=0; i<onloads.length; i++) {
//     runFunction(onloads[i]);
//   }
// }
// 
// // wont work correctly with turbolinks
// function setOnUnload(source_elem) {
//   var unloads = $( source_elem ).find(".js-onunload");
//   for (i=0; i < unloads.length; i++) {
//     var unloaded = unloads[i];
//     body = $( document ).find("body")[0];
//     js_unload_funct = $( unloaded ).attr("js_unload_funct");
//     body.onunload = window[js_unload_funct]($( unloaded ).attr('js_var'));
//   }
// }
// 
// function setOnSubmit(source_elem) {
//   console.log('set onsubmit');
//   var onsubmits = $( source_elem ).find(".js-onsubmit");
//   for (i=0; i<onsubmits.length; i++) {
//     onsubmits[i].onsubmit = setFunction;
//   }
// 
// }
// 
// function runFunction(elem) {
//   console.log('runFunction');
//   window[$( elem ).attr('js_funct')](elem, $( elem ).attr('js_var'));
// }

// function setFunction(funct_type) {
//   console.log('setFunction');
//   window[this.getAttribute('js_funct')](this);
// }

// function setSelectFunction() {
//   a_s = this[this.selectedIndex];
//   window[a_s.getAttribute('js_funct')](this, a_s.getAttribute('js_var'));
// }
